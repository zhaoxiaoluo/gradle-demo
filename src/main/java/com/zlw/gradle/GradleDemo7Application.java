package com.zlw.gradle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GradleDemo7Application {

    public static void main(String[] args) {
        SpringApplication.run(GradleDemo7Application.class, args);
    }

}
