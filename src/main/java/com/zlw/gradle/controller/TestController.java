package com.zlw.gradle.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName TestController
 * @Description TODO
 * @Author zhaoluowei
 * @Date 2020/1/27  22:35
 * @Version 1.0
 */
@Slf4j
@RestController
public class TestController {

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public Integer test() {
        log.info("test --> 我进来了 " + Thread.currentThread().getName());
        return 0;
    }


}
